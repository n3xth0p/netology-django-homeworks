from collections import Counter
from django.http.response import HttpResponse, HttpResponseNotFound
from django.shortcuts import render

# Для отладки механизма ab-тестирования используйте эти счетчики
# в качестве хранилища количества показов и количества переходов.
# но помните, что в реальных проектах так не стоит делать
# так как при перезапуске приложения они обнулятся
counter_show = Counter()
counter_click = Counter()


def index(request):
    # Реализуйте логику подсчета количества переходов с лендига по GET параметру from-landing
    from_landing = request.GET.get('from-landing')
    if from_landing != '':
        counter_click[from_landing]+=1
    return render(request, 'index.html')


def landing(request):
    # Реализуйте дополнительное отображение по шаблону app/landing_alternate.html
    # в зависимости от GET параметра ab-test-arg
    # который может принимать значения original и test
    # Так же реализуйте логику подсчета количества показов
    ab_test_arg = request.GET.get('ab-test-arg')
    
    if ab_test_arg != '':
        counter_show[ab_test_arg]+=1
    
    if ab_test_arg == 'test':
        return render(request, 'landing_alternate.html')
    elif ab_test_arg == 'original':
        return render(request, 'landing.html')
    else:
        return HttpResponseNotFound


def stats(request):
    # Реализуйте логику подсчета отношения количества переходов к количеству показов страницы
    # Для вывода результат передайте в следующем формате:

    try:
        stats_test = counter_click['test'] / counter_show['test']
    except ZeroDivisionError:
        stats_test = 0
    try:
        stats_original = counter_click['original'] / counter_show['original']
    except ZeroDivisionError:
        stats_original = 0
    
    return render(request, 'stats.html', context={
        'test_conversion': stats_test,
        'original_conversion': stats_original,
    })
