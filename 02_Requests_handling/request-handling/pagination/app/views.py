import math

from django.shortcuts import render, redirect
from django.urls import reverse
from csv import DictReader
from django.core.paginator import Paginator
from urllib.parse import urlencode


def read_bus_stations(filename, encoding_type):
    with open(filename, encoding=encoding_type) as csvfile:
        reader = DictReader(csvfile, delimiter=',')
        bus_stations_list = [{'Name': record['Name'], 'Street': record['Street'], 'District': record['District']}
                             for record in reader]
    return bus_stations_list


def index(request):
    return redirect(reverse(bus_stations))


def bus_stations(request):
    page = request.GET.get('page', 1)
    current_page = int(page)

    bus_stations_list = read_bus_stations('data-398-2018-08-30.csv', 'cp1251')
    items_per_page = 100
    total_pages = math.ceil(len(bus_stations_list) / items_per_page)
    if current_page < 1 or current_page > total_pages:
        current_page = 1
    bus_stations_on_page = bus_stations_list[(current_page - 1) * items_per_page: \
                                             current_page * items_per_page]
    prev_page_url, next_page_url = None, None
    if current_page > 1:
        prev_page_url = f"{reverse('bus_stations')}?{urlencode({'page': current_page - 1})} "
    if current_page * items_per_page < len(bus_stations_list):
        next_page_url = f"{reverse('bus_stations')}?{urlencode({'page': current_page + 1})}"
    return render(request, 'index.html', context={
        'bus_stations': bus_stations_on_page,
        'current_page': current_page,
        'prev_page_url': prev_page_url,
        'next_page_url': next_page_url,
    })

