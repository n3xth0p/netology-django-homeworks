from django import template

register = template.Library()


@register.filter
def my_get_color(value):
    if value == '' or value == '-':
        return 'white'
    if float(value) < 0:
        return 'Lime'
    elif 0 <= float(value) < 1:
        return 'white'
    elif 1 <= float(value) < 2:
        return 'IndianRed'
    elif 2 <= float(value) < 5:
        return 'Red'
    else:
        return 'DarkRed'

