from django import template

register = template.Library()


@register.filter
def my_check_empty(value):
    if value == '':
        return '-'
    else:
        return value
