from math import perm
from django.shortcuts import render
from csv import DictReader


def inflation_view(request):
    template_name = 'inflation.html'

    # чтение csv-файла и заполнение контекста
    csvfile = open('inflation_russia.csv', encoding='utf-8')
    csvdata = DictReader(csvfile, delimiter=';')

    context = {'inflation_russia': csvdata}
    return render(request, template_name, context)
