import pytest
from django.urls import reverse
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_retrieve_course(api_client, course_factory):
    course = course_factory()
    url = reverse("courses-detail", args=[course.id])
    resp = api_client.get(url)

    assert resp.status_code == HTTP_200_OK
    assert course.id == resp.json()['id']


@pytest.mark.django_db
def test_list_courses(api_client, course_factory):
    url = reverse("courses-list")
    courses = course_factory(_quantity=5)
    resp = api_client.get(url)

    assert resp.status_code == HTTP_200_OK
    assert len(courses) == len(resp.json())


@pytest.mark.django_db
def test_filter_id(api_client, course_factory):
    url = reverse("courses-list")
    courses = course_factory(_quantity=5)
    filter_id = courses[0].id
    params = {"id": filter_id}
    resp = api_client.get(url, params)

    assert resp.status_code == HTTP_200_OK
    assert resp.json()[0]['id'] == params['id']


@pytest.mark.django_db
def test_filter_name(api_client, course_factory):
    url = reverse("courses-list")
    course = course_factory(name='Математика')
    params = {'name': 'Математика'}
    resp = api_client.get(url, params)

    assert resp.status_code == HTTP_200_OK
    assert resp.json()[0]['name'] == course.name


@pytest.mark.django_db
def test_course_create(api_client):
    url = reverse("courses-list")
    payload = {
        'name': 'Математика'
    }
    resp = api_client.post(url, payload)

    assert resp.status_code == HTTP_201_CREATED
    assert resp.json()['name'] == payload['name']


@pytest.mark.django_db
def test_course_update(api_client, course_factory, student_factory):
    course = course_factory()
    new_student = student_factory()
    url = reverse("courses-detail", args=[course.id])
    payload = {
        'students': new_student.id
    }
    resp = api_client.patch(url, payload)

    assert resp.status_code == HTTP_200_OK
    assert resp.json()['students'][0] == new_student.id


@pytest.mark.django_db
def test_course_delete(api_client, course_factory):
    course = course_factory()
    url = reverse("courses-detail", args=[course.id])
    resp = api_client.delete(url)
    assert resp.status_code == HTTP_204_NO_CONTENT
