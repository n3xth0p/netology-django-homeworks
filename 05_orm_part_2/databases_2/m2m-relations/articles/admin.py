from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet

from .models import Article, Scope, ArticleScope


class ScopeInlineFormset(BaseInlineFormSet):
    def clean(self):
        main_scope_set_once = False
        for form in self.forms:
            # В form.cleaned_data будет словарь с данными
            # каждой отдельной формы, которые вы можете проверить

            #article = form.cleaned_data.get('article')
            #scope = form.cleaned_data.get('scope')
            is_main_scope = form.cleaned_data.get('is_main_scope')

            if is_main_scope:
                if not main_scope_set_once:
                    main_scope_set_once = True
                else:
                    # вызовом исключения ValidationError можно указать админке о наличие ошибки
                    # таким образом объект не будет сохранен,
                    # а пользователю выведется соответствующее сообщение об ошибке
                    raise ValidationError('Укажеите только ОДИН основной раздел')


        return super().clean()  # вызываем базовый код переопределяемого метода


class ScopeInline(admin.TabularInline):
    model = ArticleScope
    formset = ScopeInlineFormset


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    inlines = [
        ScopeInline
    ]


@admin.register(Scope)
class ScopeAdmin(admin.ModelAdmin):
    pass
