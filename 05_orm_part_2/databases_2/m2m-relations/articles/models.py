from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=256, verbose_name='Название')
    text = models.TextField(verbose_name='Текст')
    published_at = models.DateTimeField(verbose_name='Дата публикации')
    image = models.ImageField(null=True, blank=True, verbose_name='Изображение', )

    scope = models.ManyToManyField('Scope', through='ArticleScope', verbose_name='Раздел')

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

    def __str__(self):
        return f'{self.id} - {self.title} - {self.scope.all()}'


class Scope(models.Model):
    name = models.CharField(max_length=100, verbose_name='Раздел')

    def __str__(self):
        return f'{self.id} - {self.name}'

    class Meta:
        verbose_name = 'Раздел'
        verbose_name_plural = 'Разделы'
        ordering = ['-articlescope__is_main_scope']


class ArticleScope(models.Model):
    article = models.ForeignKey('Article', on_delete=models.CASCADE, verbose_name='Статья')
    scope = models.ForeignKey('Scope', on_delete=models.CASCADE, verbose_name='Раздел')
    is_main_scope = models.BooleanField(default=False, verbose_name='Основной раздел')

    def __str__(self):
        return f'{self.id} - {self.article} - {self.scope} - {self.is_main_scope}'
