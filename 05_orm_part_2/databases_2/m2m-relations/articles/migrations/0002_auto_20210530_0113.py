# Generated by Django 3.1.2 on 2021-05-29 22:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='scope',
            field=models.ManyToManyField(through='articles.ArticleScope', to='articles.Scope', verbose_name='Раздел'),
        ),
        migrations.AlterField(
            model_name='articlescope',
            name='article',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='articles.article', verbose_name='Статья'),
        ),
        migrations.AlterField(
            model_name='articlescope',
            name='is_main_scope',
            field=models.BooleanField(default=False, verbose_name='Основной раздел'),
        ),
        migrations.AlterField(
            model_name='articlescope',
            name='scope',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='articles.scope', verbose_name='Раздел'),
        ),
        migrations.AlterField(
            model_name='scope',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Раздел'),
        ),
    ]
