from django import template

register = template.Library()


@register.filter
def check_is_main(scope, article):
    return scope.articlescope_set.get(article=article).is_main_scope
