from django.views.generic import ListView
from django.shortcuts import render

from .models import Student, Teacher


def students_list(request):
    template = 'school/students_list.html'
    # object_list = Student.objects.all()
    # teachers = Teacher.objects.all()
    # s = Teacher.objects.first().student_set.all()
    # studets_teachers = Student.objects.all()
    # print(studets_teachers)
    # print(s)

    # используйте этот параметр для упорядочивания результатов
    # https://docs.djangoproject.com/en/2.2/ref/models/querysets/#django.db.models.query.QuerySet.order_by
    ordering = 'group'

    object_list = Student.objects.all().order_by(ordering)
    teachers = Student.objects.last().teacher.all()
    print(teachers)

    context = {'object_list': object_list}


    return render(request, template, context)
