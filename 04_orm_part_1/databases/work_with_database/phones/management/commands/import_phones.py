from csv import DictReader

from django.core.management.base import BaseCommand
from phones.models import Phone
from django.template.defaultfilters import slugify


class Command(BaseCommand):
    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        with open('phones.csv', 'r') as csvfile:
            phone_reader = DictReader(csvfile, delimiter=';')

            for line in phone_reader:
                # TODO: Добавьте сохранение модели
                #
                phone_item = Phone(name=line['name'], image=line['image'], price=line['price'],
                                   release_date=line['release_date'], lte_exists=line['lte_exists'],
                                   name_slug=slugify(line['name']))
                phone_item.save()
