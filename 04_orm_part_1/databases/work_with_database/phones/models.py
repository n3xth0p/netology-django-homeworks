from django.db import models


class Phone(models.Model):
    # TODO: Добавьте требуемые поля
    name = models.TextField(null=False)
    image = models.URLField()
    price = models.FloatField(null=False)
    release_date = models.DateField()
    lte_exists = models.BooleanField(null=False)
    name_slug = models.SlugField(null=False)

