from django.shortcuts import render
from phones.models import Phone
from django.urls import reverse
from urllib.parse import urlencode
from django.http import HttpResponseRedirect


def show_catalog(request):
    template = 'catalog.html'
    sort_method = request.GET.get('sort')
    if sort_method == 'name':
        phones_list = Phone.objects.all().order_by('name')
    elif sort_method == 'min_price':
        phones_list = Phone.objects.all().order_by('price')
    elif sort_method == 'max_price':
        phones_list = Phone.objects.all().order_by('-' + 'price')
    else:
        phones_list = Phone.objects.all()

    sort_links = {
        'названию': f"{reverse('catalog')}?{urlencode({'sort': 'name'})}",
        'начиная с дешевых': f"{reverse('catalog')}?{urlencode({'sort': 'min_price'})}",
        'начиная с дорогих': f"{reverse('catalog')}?{urlencode({'sort': 'max_price'})}",
    }

    context = {
        'sort_links': sort_links,
        'phones_list': phones_list
    }
    return render(request, template, context)


def show_product(request, slug):
    template = 'product.html'
    slug_list = Phone.objects.values_list('name_slug', flat=True)

    if slug in slug_list:
        phone = Phone.objects.filter(name_slug=slug)[0]
    else:
        return HttpResponseRedirect('/catalog')

    context = {'phone': phone}

    return render(request, template, context)
